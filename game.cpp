#include <iostream>
#include <stdlib.h>
#include "game.h"
#include <string>

using namespace std;

//Global variables:
int Answer[4]{}; 

int AmountOfTries = 1;

int UserInput[4]{};
bool AnswersCorrect[4]{false, false, false, false};

void playGame()
{
    srand((unsigned)time(0)); //Hacer la funci�n Rand no repetir los n�meros aleatorios

    generateAnswer();

    cout << "Game start!\n\n";

    cout << "Try to write a number of 4 digits remembering that:\n";
    cout << "C means that a value and position of a number are right\n";
    cout << "T means that a value is right but position is incorrect\n";
    cout << "F means that this number is not contained\n\n";

    //showJoseo();    

    for (int i = 0; i < 10; i++)
    {
        readUserInput();

        createNumericRow(UserInput);
        createEvaluatingRow();

        if (10 - AmountOfTries == 1) // cuando es = a 1, en realidad, fueron hecho 10 intentos
        {
            cout << "\nYOU HAVE LOOOOST!\n";
            cout << "El numero correcto era \n";
            createNumericRow(Answer);
            exit(0);
        }

        if (didPlayerWin())
        {
            cout << "You WON with " << 10 - AmountOfTries << " tries left";
            return;
        }

        else
        {
            cout << "Ooooops, wrong! " << 10 - AmountOfTries << " tries left.\n\n";

            AmountOfTries += 1;
        }
    }
}
int genNumber(int nMax)
{
    return rand() % (nMax); //en realidad, hasta nMax - 1
}

void generateAnswer()
{
    for (int i = 0; i < 4; i++)
    {
        Answer[i] = genNumber(10);
    }
}

void readUserInput() {
    string input;
    string validInputs[10] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    
    for (int i = 0; i < 4; i++)
    {
        do 
        {
            cout << "Enter number " << i + 1 << ": \n";
            cin >> input;
        } while (!validateInput(input, validInputs)); //!= porque ciclo continua cuando condicion es verdadera
        
        UserInput[i] = stoi(input);  //stoi converts string to int       
    }
}

void showJoseo() { //mostrar la respuesta para las pruebas
    cout << "Answer: \n";
    createNumericRow(Answer);
    cout << endl;
}

bool validateInput(string input, string validInputs[10])
{
    for (int i = 0; i < 10; i++)
    {
        if (input == validInputs[i])
        {            
            return true;
        }
    }

    return false;
}


void createNumericRow(int rowToGenerate[4])
{
    cout << "|";
    for (int i = 0; i < 4; i++) cout << " " << rowToGenerate[i] << " |";    
    cout << endl;
}

void createEvaluatingRow()
{
    cout << "|";
    for (int i = 0; i < 4; i++)
    {
        char letterToType = validate(i);
        cout << " " << letterToType << " |";
    }
    cout << endl;
}


char validate(int elementNumber)
{
    if (UserInput[elementNumber] == Answer[elementNumber])
    {
        AnswersCorrect[elementNumber] = true;
        return 'C';
    }

    for (int i = 0; i < 4; i++)
    {
        if (UserInput[elementNumber] == Answer[i])
        {
            AnswersCorrect[elementNumber] = false;
            return 'T';
        }
    }

    AnswersCorrect[elementNumber] = false;
    return 'F';
    
}

bool didPlayerWin()
{
    if (AnswersCorrect[0] == true && AnswersCorrect[1] == true &&
        AnswersCorrect[2] == true && AnswersCorrect[3] == true) return true;

    return false;
}